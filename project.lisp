(in-package #:jise)

(defvar *has-more-message* "This project has more issues that have been hidden.
You can still list folders if the issue exists, even if the folder doesn't exist.
")

(defun project-list ()
  (loop for project in (jira-list-projects)
       collect (key project)))

(defun project-issues-by-id (project-key)
  (multiple-value-bind (issues has-more)
      (jira-list-project-issues project-key 500 "resolution = unresolved")
    (append (mapcar #'(lambda (issue) (key issue))
		    issues)
	    (when has-more '("README")))))

(defun project-issues-readme ()
  *has-more-message*)

(defun project-issues-readme-size ()
  (length *has-more-message*))

(defun project-issues-search (project-key search)
  (with-store-value ((format nil "search-~a-~a"
		      project-key
		      search)
		     3600)
    (handler-case
	(mapcar #'key (jira-list-project-issues project-key 100 search))
      (jira-condition () nil))))

(defun project-saved-searches (project-key)
  (store-load (format nil "~a-searches" project-key)))

(defun project-save-search (project-key search)
  (let ((saved-searches (project-saved-searches project-key)))
    (unless (member search saved-searches :test #'equal)
      (store-save (format nil "~a-searches" project-key)
		  (append saved-searches (list search))))))

(defun project-delete-search (project-key search)
  (let ((saved-searches (project-saved-searches project-key)))
    (when (member search saved-searches :test #'equal)
      (let ((key (format nil "~a-searches" project-key)))
	(if (= (length saved-searches) 1)
	    (store-delete key)
	    (store-save key (remove search saved-searches :test #'equal)))))))

(defun project-search-exists (project-key search)
  (member search (project-saved-searches project-key) :test #'equal))
