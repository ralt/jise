(defsystem jise
    :name "jise"
    :author "Florian Margaine <florian@margaine.com>"
    :description "( ͡° ͜ʖ ͡°)"
    :depends-on ("babel"
		 "cffi"
		 "cl-fuse-meta-fs"
		 "cl-ppcre"
		 "drakma"
		 "jsown"
		 "marshal"
		 "optima")
    :components ((:file "package")
		 (:file "xattr")
		 (:file "store")
		 (:file "jira")
		 (:file "project")
		 (:file "issue")
		 (:file "jise"))
    :build-operation "asdf:program-op"
    :build-pathname "jise"
    :entry-point "jise:main")
