(in-package #:jise)

(defun issue-cached-issue (issue-key)
  (with-store-value ((format nil "issue-~a" issue-key) (* 3600 24))
    (jira-fetch-issue issue-key)))

(defun issue-exists (issue-key)
  (handler-case
      (progn
	(jira-fetch-issue issue-key)
	t)
    (jira-condition ()  nil)))

(defun issue-list ()
  '("README.md" "comments" "assignee"))

(defun issue-readme (issue-key)
  (let ((issue (issue-cached-issue issue-key)))
    (format nil "# ~a~%~%~a~%" (summary issue) (description issue))))

(defun issue-readme-size (issue-key)
  (length (issue-readme issue-key)))

(defun issue-assignee (issue-key)
  (let* ((issue (issue-cached-issue issue-key))
	 (assignee (assignee issue)))
    (when assignee
      (format nil "~a <~a>~%" (name assignee) (email assignee)))))

(defun issue-assignee-size (issue-key)
  (length (issue-assignee issue-key)))

(defun issue-assign (issue-key text)
  (cond ((equalp text #(109 101 10)) ; "me\n"
	 (jira-issue-assign-myself issue-key))
	((equalp text #(110 111 110 101 10)) ; "none\n"
	 (jira-unassign-issue issue-key))
	(t (jira-issue-edit-assignee
	    issue-key
	    (babel:octets-to-string text)))))
