(in-package #:jise)

(push (cons "application" "json") drakma:*text-content-types*)

(define-condition jira-condition (condition)
  ((message :type string :initarg :message :reader message))
  (:report (lambda (condition stream)
	     (format stream "~a" (message condition)))))

(defclass jira-assignee ()
  ((name :initarg :name :reader name)
   (email :initarg :email :reader email)))

(defclass jira-issue ()
  ((key :initarg :key :reader key)
   (summary :initarg :summary :reader summary)
   (description :initarg :description :reader description)
   (assignee :initarg :assignee :reader assignee)
   (issuetype :initarg :issuetype :reader issuetype)))

(defmethod print-object ((issue jira-issue) out)
  (format out "~a / ~a / ~a: ~a"
	  (key issue)
	  (let ((assignee (assignee issue)))
	    (if assignee
		(name assignee)
		"nobody"))
	  (issuetype issue)
	  (summary issue)))

(defclass jira-project ()
  ((key :initarg :key :reader key)
   (name :initarg :name :reader name)))

(defun jira-description (description)
  (when (jsown:keyp description "content")
    (let ((paragraphs (jsown:val description "content")))
      (format
       nil
       "~{~a~^~%~%~}"
       (loop for paragraph in paragraphs
	  collect (apply #'concatenate
			 'string
			 (loop for text in (jsown:val paragraph "content")
			    when (string= (jsown:val text "type") "text")
			    collect (jsown:val text "text"))))))))

(defun jira-hydrate-issue (raw-issue)
  (let ((fields (jsown:val raw-issue "fields")))
    (make-instance 'jira-issue
		   :key (jsown:val raw-issue "key")
		   :issuetype (jsown:val (jsown:val fields "issuetype") "name")
		   :summary (jsown:val fields "summary")
		   :description (jira-description
				 (jsown:val fields "description"))
		   :assignee (let ((assignee-field (jsown:val fields "assignee")))
			       (when assignee-field
				 (make-instance
				  'jira-assignee
				  :name (jsown:val assignee-field "displayName")
				  :email (jsown:val assignee-field "emailAddress")))))))

(defun jira-fetch (url)
  (let ((response
	 (jsown:parse
	  (drakma:http-request
	   url
	   :basic-authorization (list *jira-user*
				      *jira-token*)))))
    (if (jsown:keyp response "errors")
	(error 'jira-condition
		:message (jsown:val response "errorMessages"))
	response)))

(defun jira-search (project &optional (max 100) jql)
  (let ((issues)
	(start 0)
	(max-results (min max 50))
	(total 0))
    (loop
       (let ((results (jsown:parse
		       (drakma:http-request
			(format
			 nil
			 "~a/rest/api/3/search"
			 *jira-url*)
			:method :post
			:content-type "application/json"
			:basic-authorization (list *jira-user* *jira-token*)
			:content
			(jsown:to-json
			 `(:obj
			   ("maxResults" . ,max-results)
			   ("startAt" . ,start)
			   ("fields" . ("key"
					"summary"
					"description"
					"assignee"
					"issuetype"))
			   ("jql" . ,(format
				      nil
				      "project=~a~a"
				      project
				      (if jql
					  (format nil " and ~a" jql)
					  "")))))))))
	 (when (jsown:keyp results "errors")
	   (error 'jira-condition
		  :message (jsown:val results "errorMessages")))
	 (setf total (jsown:val results "total"))
	 (dolist (result (jsown:val results "issues"))
	   (push result issues))
	 (when (> start max)
	   (return-from jira-search (values
				     issues
				     (> total (length issues)))))
	 (incf start max-results)))))

(defun jira-fetch-issue (issue-key)
  (let ((raw-issue (jira-fetch
		    (format
		     nil
		     "~a/rest/api/3/issue/~a?fields=summary,description,assignee,issuetype"
		     *jira-url*
		     issue-key))))
    (jira-hydrate-issue raw-issue)))

(defun jira-issue-edit-assignee (issue-key assignee)
  (drakma:http-request
   (format
    nil
    "~a/rest/api/3/issue/~a/assignee"
    *jira-url*
    issue-key)
   :method :put
   :content-type "application/json"
   :basic-authorization (list *jira-user*
			      *jira-token*)
   :content (jsown:to-json
	     `(:obj
	       ("name" . ,(subseq assignee 0 (1- (length assignee))))))))

(defun jira-issue-assign-myself (issue-key)
  (drakma:http-request
   (format
    nil
    "~a/rest/api/3/issue/~a/assignee"
    *jira-url*
    issue-key)
   :method :put
   :content-type "application/json"
   :basic-authorization (list *jira-user*
			      *jira-token*)
   :content (jsown:to-json
	     `(:obj
	       ("accountId" . ,*jira-account-id*)))))

(defun jira-unassign-issue (issue-key)
  (jira-issue-edit-assignee issue-key (format nil "-1~%")))

(defun jira-list-projects ()
  (loop for project in (jsown:val
			(jira-fetch
			 (format
			  nil
			  "~a/rest/api/3/project/search"
			  *jira-url*))
			"values")
     collect (make-instance 'jira-project
			    :key (jsown:val project "key")
			    :name (jsown:val project "name"))))

(defun jira-list-project-issues (project &optional (max 100) jql)
  (let ((results (mapcar #'jira-hydrate-issue (jira-search project max jql))))
    (values results (length results))))
