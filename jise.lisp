(in-package #:jise)

(defvar *jira-url* nil)
(defvar *jira-account-id* nil)
(defvar *jira-user* nil)
(defvar *jira-token* nil)

(defun directory-content (path)
  (match path
    (nil (project-list))
    ((list _) '("by-search" "by-id"))
    ((list project-key "by-id") (project-issues-by-id project-key))
    ((list _ "by-id" _) (issue-list))
    ((list project-key "by-search") (project-saved-searches project-key))
    ((list project-key "by-search" search) (project-issues-search project-key
								  search))
    ((list _ "by-search" _ _) (issue-list))
    (_ nil)))

(defun directory-p (path)
  (match path
    (nil t)
    ((list _) t)
    ((list _ _) t)
    ((list _ "by-id" "README") nil)
    ((list _ "by-id" _) t)
    ((list _ "by-id" _ "README.md") nil)
    ((list _ "by-id" issue-key "comments") (issue-exists issue-key))
    ((list project-key "by-search" search) (project-search-exists project-key
								  search))
    ((list project-key "by-search" search _) (project-search-exists
					      project-key
					      search))
    ((list _ "by-search" _ _ "README.md") nil)
    ((list project-key "by-search" search issue-key "comments")
     (and (project-search-exists project-key search)
	  (issue-exists issue-key)))))

(defun symlink-target (path)
  nil)

(defun file-open (path flags)
  0)

(defun file-release (path flags)
  0)

(defun file-read (path size offset fh)
  (declare (ignore fh))
  (let ((content
	 (match path
	   ((list _ "by-id" "README") (project-issues-readme))
	   ((list _ "by-id" issue-key "README.md") (issue-readme issue-key))
	   ((list _ "by-id" issue-key "assignee") (issue-assignee issue-key))
	   ((list _ "by-search" _ issue-key "README.md") (issue-readme
							  issue-key))
	   ((list _ "by-search" _ issue-key "assignee") (issue-assignee
							 issue-key))
	   (_ ""))))
    (subseq content offset (+ offset (min size (length content))))))

(defun file-size (path)
  (match path
    ((list _ "by-id" "README") (project-issues-readme-size))
    ((list _ "by-id" issue-key "README.md") (issue-readme-size issue-key))
    ((list _ "by-id" issue-key "assignee") (issue-assignee-size issue-key))
    ((list _ "by-search" _ issue-key "README.md") (issue-readme-size
						   issue-key))
    ((list _ "by-search" _ issue-key "assignee") (issue-assignee-size
						  issue-key))))

(defun file-executable-p (path)
  nil)

(defun file-flush (path fh)
  0)

(defun file-create (path mode dev)
  (match path
    ((list _ "by-id" _ "assignee") t)
    (_ nil)))

(defun file-write (path data offset fh)
  (match path
    ((list _ "by-id" issue-key "assignee") (issue-assign issue-key data)))
  t)

(defun file-writeable-p (path)
  (match path
    ((list _ "by-id" _ "assignee") t)
    (_ nil)))

(defun file-truncate (path offset)
  0)

(defun mkdir (path mode)
  (declare (ignore mode))
  (match path
    ((list project-key "by-search" search)
     (progn
       (project-save-search project-key search)
       0))
    (_ (- cl-fuse:error-EACCES))))

(defun unlink (path)
  (- cl-fuse:error-EACCES))

(defun rmdir (path)
  (match path
    ((list project-key "by-search" search)
     (progn
       (project-delete-search project-key search)
       0))
    (_ (- cl-fuse:error-EACCES))))

(defun main ()
  (let ((config-file (uiop:xdg-config-home "jise" "rc")))
    (when (probe-file config-file)
      (load config-file)))

  (unless *store-directory*
    (setf *store-directory* (ensure-directories-exist
			     (uiop:xdg-data-home "jise" "db" "unused"))))

  (cl-fuse:fuse-run `("jise"
		      ,(second uiop:*command-line-arguments*)
		      "-odebug")
		    :directory-content 'directory-content
		    :directoryp 'directory-p
                    :symlink-target 'symlink-target
                    :symlinkp 'symlink-target
                    :file-open 'file-open
                    :file-release 'file-release
                    :file-read 'file-read
                    :file-size 'file-size
                    :file-write 'file-write
                    :file-write-whole nil
		    :file-create 'file-create
                    :file-writeable-p 'file-writeable-p
                    :file-executable-p 'file-executable-p
                    :file-create nil
                    :chmod nil
                    :chown nil
                    :truncate 'file-truncate
                    :file-flush 'file-flush
                    :mkdir 'mkdir
                    :unlink 'unlink
                    :rmdir 'rmdir
                    :symlink nil
                    :rename nil))
