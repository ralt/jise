(in-package #:jise)

;;;; string-only xattrs CFFI.

(cffi:defcfun (%setxattr "setxattr") :int
  (path :string)
  (name :string)
  (value :pointer)
  (size :unsigned-int)
  (flags :int))

(defun setxattr (path name value type size)
  (cffi:with-foreign-object (value-pointer :pointer 1)
    (setf (cffi:mem-ref value-pointer type) value)
    (%setxattr path name value-pointer size 0)))

(cffi:defcvar "errno" :int)

(cffi:defcfun (strerror "strerror") :string
  (errno :int))

(defun getxattr (path name type size)
  (cffi:with-foreign-object (value :string size)
    (let ((value-size (cffi:foreign-funcall "getxattr"
					    :string path
					    :string name
					    :pointer value
					    :unsigned-int size
					    :int)))
      (if (< value-size 0)
	  (error (strerror *errno*))
	  (cffi:mem-ref value type)))))
