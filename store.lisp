(in-package #:jise)

(defvar *store-directory* nil)
(defvar *ttl* "user.ttl")
(defvar *date* "user.date")

(defun store-save (key value &optional (ttl 0))
  (let ((path (namestring (merge-pathnames key *store-directory*))))
    (with-open-file (stream
		     path
		     :direction :output
		     :if-does-not-exist :create
		     :if-exists :overwrite)
      (write-sequence (store-serialize value) stream)
      (setxattr path *ttl* ttl :unsigned-int 4)
      (setxattr path *date* (get-universal-time) :unsigned-int 4))))

(defun store-load (key)
  (let ((path (namestring (merge-pathnames key *store-directory*))))
    (when (probe-file path)
      (let ((ttl (getxattr path *ttl* :unsigned-int 4))
	    (time (getxattr path *date* :unsigned-int 4)))
	(when (or (= ttl 0)
		  (< (get-universal-time) (+ ttl time)))
	  (with-open-file (stream path)
	    (let ((data (make-array (file-length stream)
				    :element-type 'character)))
	      (read-sequence data stream)
	      (store-deserialize data))))))))

(defun store-delete (key)
  (delete-file (merge-pathnames key *store-directory*)))

(defun store-serialize (value)
  (write-to-string (ms:marshal value)))

(defun store-deserialize (string)
  (ms:unmarshal (read-from-string string)))

(defmacro with-store-value ((key ttl) &body body)
  (let ((cached (gensym))
	(result (gensym)))
    `(let ((,cached (store-load ,key)))
       (if ,cached
	   ,cached
	   (let ((,result ,@body))
	     (store-save ,key ,result ,ttl)
	     ,result)))))
